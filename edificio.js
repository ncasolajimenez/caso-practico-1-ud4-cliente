class Edificio {
  constructor(calle, numero, CP) {
    this.calle = calle;
    this.numero = numero;
    this.CP = CP;
    this.plantas = [];
  }
  agregarPlantasyPuertas(numplantas, puertas) {
    var plantas = [];
      for(let j = 1; j <= numplantas; j++){
          plantas.push({
              numero: j,
              puertas: []
          });
          for(let i = 1; i <= puertas; i++){
              plantas[j-1].puertas.push({
                  numero: i,
                  propietario: "VACIO"
              });
          }
      }
      this.plantas = [...this.plantas, ...plantas];
  }
  modificarNumero(nuevoNumero) {
    this.numero = nuevoNumero;
  }
  modificarCalle(nuevaCalle) {
    this.calle = nuevaCalle;
  }
  modificarCodigoPostal(nuevoCP) {
    this.CP = nuevoCP;
  }
  mostrarCalle() {
    return this.calle;
  }
  mostrarNumero() {
    return this.numero;
  }
  mostrarCodigoPostal() {
    return this.CP;
  }
  agregarPropietario(nombre, planta, puerta) {
    this.plantas[planta-1].puertas[puerta-1].propietario = nombre;
  }
  mostrarPlantas() {
    return this.plantas;
  }
}

function makeTable(array) {
    var table = document.createElement('table');
    var num_puertas = document.getElementById('puertas').value;
        var rowHead = document.createElement('tr');
        var cellHead = document.createElement('th');
        cellHead.textContent = "Piso";
        rowHead.appendChild(cellHead);
        for (var j = 1; j <= num_puertas; j++) {
            var cell = document.createElement('th');
            cell.textContent = `Puerta nº ${j}`;
            rowHead.appendChild(cell);
        }
        table.appendChild(rowHead);
    for (var i = 0; i < array.length; i++) {
        var row = document.createElement('tr');
        var cellHead = document.createElement('th');
        cellHead.textContent = `Piso nº ${i+1}`;
        row.appendChild(cellHead);
        for (var j = 0; j < array[i].puertas.length; j++) {
            var cell = document.createElement('td');
            cell.textContent = array[i].puertas[j].propietario;
            row.appendChild(cell);
        }
        table.appendChild(row);
    }
    table.setAttribute("class", "table table-bordered");
    return table;
}

function generateTable(plantilla, plantas) {
  var resultado = document.getElementById("resultado");
  var datos = document.getElementById("datos");
  resultado.innerHTML = '';
  if(plantilla != false) {
    datos.innerHTML = plantilla;
  }
  resultado.appendChild(makeTable(plantas));
}

document.getElementById("crear").addEventListener("click", function( event ) {
  event.preventDefault();
  var calle = document.getElementById('calle').value;
  var numero = document.getElementById('numero').value;
  var cp = document.getElementById('CP').value;
  var plantas = document.getElementById('plantas').value;
  var puertas = document.getElementById('puertas').value;
  if (typeof window.miedificio == "undefined") {
    window.miedificio = new Edificio(calle, numero, cp);
  } else {
    window.miedificio.modificarNumero(numero);
    window.miedificio.mostrarCalle(calle);
    window.miedificio.modificarCodigoPostal(cp);
  }
  window.miedificio.agregarPlantasyPuertas(plantas, puertas);
  var f_plantas = window.miedificio.mostrarPlantas();
  var plantilla = `
  <div class="alert alert-success" role="alert">
    <h4 class="alert-heading">Edificio en: Calle ${calle} nº ${numero}, CP ${cp}</h4>
  </div>
`;
  generateTable(plantilla, f_plantas);
}, false);

document.getElementById("modificar").addEventListener("click", function( event ) {
event.preventDefault();
  if (typeof window.miedificio == "undefined") {
    alert("Crea primero el edificio");
    return false;
  }
  var nombre = document.getElementById('nombre').value;
  var piso = document.getElementById('piso').value;
  var puerta = document.getElementById('puerta').value;
  window.miedificio.agregarPropietario(nombre, piso, puerta);
  var f_plantas = window.miedificio.mostrarPlantas();
  generateTable(false, f_plantas);
}, false);
